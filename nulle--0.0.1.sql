-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION nulle" to load this file. \quit

CREATE FUNCTION nulle ()
RETURNS text
LANGUAGE sql immutable strict
as $$
 SELECT null;
$$

